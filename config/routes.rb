Rails.application.routes.draw do
  # devise_for :admins

  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout'}

  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  get 'welcome/index'


  resources :contacts, :only => :create

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
