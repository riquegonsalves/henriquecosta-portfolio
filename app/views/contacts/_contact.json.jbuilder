json.extract! contact, :id, :name, :phone, :email, :company, :message, :created_at, :updated_at
json.url contact_url(contact, format: :json)
