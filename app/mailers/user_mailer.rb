class UserMailer < ApplicationMailer
	default from: 'Henrique Costa <contato@henriquecosta.com>'
 
  def contact_mail(contact)
    @contact = contact
    mail(to: 'contato@henriquecosta.com', subject: '[Henrique Costa] Contato do Site')
  end

  def respond_mail(contact)
    @contact = contact
    mail(to: @contact.email, subject: '[Henrique Costa] Obrigado por entrar em contato.')
  end


end
