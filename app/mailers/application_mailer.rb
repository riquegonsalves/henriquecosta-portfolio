class ApplicationMailer < ActionMailer::Base
  default from: 'contato@henriquecosta.com'
  layout 'mailer'
end
